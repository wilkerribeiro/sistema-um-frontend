import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProtegidasRoutingModule } from './protegidas-routing.module';
import { ProtegidoComponent } from './components/protegido.component';

@NgModule({
	imports: [
		CommonModule,
		ProtegidasRoutingModule
	],
	declarations: [ProtegidoComponent]
})
export class ProtegidasModule { }
