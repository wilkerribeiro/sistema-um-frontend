import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProtegidoComponent } from './components/protegido.component';

const routes: Routes = [
	{
		path: '', children: [
			{ path: '', component: ProtegidoComponent }
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ProtegidasRoutingModule { }
