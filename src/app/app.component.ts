import { Http } from '@angular/http';
import { Component } from '@angular/core';
import { AuthService } from 'angular-auth-module';
import { Router } from '@angular/router';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'app';
	constructor(private authService: AuthService, private router: Router, private http: Http) { }
	redirect() {
		this.http.get('https://localhost:8092' + '/redirect').subscribe((response) => {
			console.log('response.json()');
		});
	}
	logout() {
		this.authService.logout();
	}
	protegido() {
		this.router.navigate(['/protegido']);
	}
}
