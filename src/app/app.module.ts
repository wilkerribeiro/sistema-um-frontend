import { AuthGuard, AuthService } from 'angular-auth-module';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Route, RouterModule } from '@angular/router';

const appRoutes: Route[] = [
	{ path: 'protegido', canActivate: [AuthGuard], loadChildren: './modules/protegidas/protegidas.module.ts#ProtegidasModule' },
	{ path: 'auth', loadChildren: 'angular-auth-module#AuthModule' }
];


@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		HttpModule,
		RouterModule.forRoot(appRoutes)
	],
	providers: [AuthService, AuthGuard],
	bootstrap: [AppComponent]
})
export class AppModule {
	constructor(private authService: AuthService) {
		this.authService.sistemaKey = 'sistema-um';
		this.authService.ssoUrl  = 'https://localhost:4200';
	}
}
